<?php

use libJD_php\Collections\Collection;
use libJD_php\Templating\Views;

class PagesController
{
    public static function Root()
    {
        header('Location: /home');
    }

    public static function Home()
    {
        Views::render('master#pages.static.home', 'TTE', new Collection());
    }
}
