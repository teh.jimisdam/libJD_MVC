<?php

include_once '../autoload.php';

use libJD_php\Collections\Collection;
use libJD_php\Routing\Route;
use libJD_php\Routing\Router;
use libJD_php\Templating\Template;
use libJD_php\Templating\TemplateEngine;
use libJD_php\Templating\Views;


Views::setRootDirectory('../App/Views');
new TemplateEngine('TTE', ['InfoPass']);

new Template('InfoPass',
    function (string $content, Collection &$data)
    {
        foreach ($data as $key => &$value)
            $content = preg_replace("#{{{$key}}}#", $value, $content);

        return $content;
    });

echo "<pre>";
echo "</pre><hr />";


(new Router([
    new Route('', 'GET', 'Pages#Root'),
    new Route('home', 'GET', 'Pages#Home'),
]))->listen();
