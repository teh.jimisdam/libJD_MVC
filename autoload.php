<?php

#   Libraries
require_once '../Core/libJD_php/Collections/Collection.php';
require_once '../Core/libJD_php/Templating/Renderable.php';
require_once '../Core/libJD_php/Templating/Template.php';
require_once '../Core/libJD_php/Templating/TemplateEngine.php';
require_once '../Core/libJD_php/Templating/Views.php';
require_once '../Core/libJD_php/Databases/Database.php';
require_once '../Core/libJD_php/Databases/MySQL_Driver.php';
require_once '../Core/libJD_php/Databases/Modelling/Model.php';
require_once '../Core/libJD_php/Routing/Route.php';
require_once '../Core/libJD_php/Routing/Router.php';

#   Models

#   Controllers
require_once '../App/Controllers/PagesController.php';
